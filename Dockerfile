# Get sonarqube official Docker image from Dockerhub
FROM sonarqube:7.6-community

# Copy the custom plugin .jar into the the Docker image at the right location for the plugin to be loaded
COPY target/java-custom-rules-1.0-SNAPSHOT.jar /opt/sonarqube/extensions/plugins



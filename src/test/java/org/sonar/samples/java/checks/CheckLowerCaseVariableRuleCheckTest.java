// Ex�cution des cas de tests d�finis dans le fichier de test CheckLowerCaseVariableRuleCheck.java

package org.sonar.samples.java.checks;

import org.junit.Test;
import org.sonar.java.checks.verifier.JavaCheckVerifier;

public class CheckLowerCaseVariableRuleCheckTest {

    @Test
    public void test() {

        /*
         * Pour all�ger l'appel de JavaCheckVerifier.verify nous pouvons choisir
         * d'instancier CheckLowerCasVariableRuleCheck en amont mais ce n'est
         * pas obligatoire
         */
        JavaCheckVerifier.verify( "src/test/files/CheckLowerCaseVariableRuleCheck.java",
                new CheckLowerCaseVariableRuleCheck() );
    }

}

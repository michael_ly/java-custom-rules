// Code source permettant d'implémenter la règle CheckLowerCaseVariableRule
// A partir d'un arbre contenant toutes les variables déclarées dans le code, chaque nom de variable est étudié pour reconnaitre la case de sa première lettre et éventuellement signaler l'erreur de nommage
// Modification de l'erreur ... rapportée dans le ticket LINTY-18

package org.sonar.samples.java.checks;

import java.util.List;

import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.Tree.Kind;
import org.sonar.plugins.java.api.tree.VariableTree;

import com.google.common.collect.ImmutableList;

/* IssuableSubscriptionVisitor permet d'utiliser reportIssue
 * en cas de non respect de la règle pour pouvoir la rapporter
 */

@Rule( key = "CheckLowerCaseVariableRule", name = "Check Lower Case Variable Rule", description = "A variable name shouldn't start with an upper case character" )
public class CheckLowerCaseVariableRuleCheck extends IssuableSubscriptionVisitor {

    @Override
    public List<Kind> nodesToVisit() {
        // Récupération des noeuds de type VARIABLE dont nous allons
        // sélectionner les noms
        return ImmutableList.of( Kind.VARIABLE );
    }

    @Override
    public void visitNode( Tree tree ) {
        VariableTree variable = (VariableTree) tree;
        // Si le premier caractère du nom est une majuscule ...
        if ( variable.simpleName().toString().charAt( 0 ) == variable.simpleName().toString().toUpperCase()
                .charAt( 0 ) ) {
            // Un rapport d'erreur est noté
            reportIssue( variable.simpleName(), "First character in variable name is an upper case" );
        }
    }
}

// Impl�mentation des cas de tests permettant de v�rfier l'impl�mentation correcte de la r�gle au regard des exigences auxquelles elle r�pond
class CheckLowerCaseVariableRuleCheck {
    
    /* Les valeurs des variables ne sont pas importantes
     * Nous testons ici leur nom
     */
    
    int foo1 = 3;
    int Foo2 = 4; // Noncompliant {{First character in variable name is an upper case}}
    
    /* La premi�re lettre est une minuscule
     * mais les deux suivantes sont majuscules
     * permettant de tester le bon fonctionnement de la d�tection de la premi�re lettre
     */
    int fOO3 = 5;
    
}